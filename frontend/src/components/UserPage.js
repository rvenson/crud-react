import React from 'react'
import axios from 'axios'
import UserItem from './UserItem'
import UserForm from './UserForm'
import Grid from '@material-ui/core/Grid'
import AppBar from '@material-ui/core/AppBar'
import List from '@material-ui/core/List'
import ListSubheader from '@material-ui/core/ListSubheader'
import 'typeface-roboto';

class UserPage extends React.Component {

    constructor(props) {
        super(props)
        this.BASE_URL = "http://localhost:4000/api/users"
        this.state = { users: [], selectedUser: undefined }
        this.loadUsers()
    }

    loadUsers = () => {
        axios.get(this.BASE_URL).then((response) => {
            this.setState({ users: response.data })
        }).catch((error) => {
            console.error(error)
        })
    }

    handleAction = (data) => {
        if (this.state.selectedUser) {
            var id = this.state.selectedUser._id
            this.handleUpdate(id, data)
        } else {
            this.handleInsert(data)
        }
    }

    selectUser = (user) => {
        if (this.state.selectedUser) {
            var user = this.state.selectedUser._id == user._id ? undefined : user
        }

        this.setState({ selectedUser: user })
    }

    handleUpdate = (id, data) => {
        console.log(data)
        axios.put(this.BASE_URL + "/" + id, data).then(() => {
            this.loadUsers()
        }).catch((error) => {
            console.error(error)
        })
    }

    handleInsert = (data) => {
        console.log(data)
        axios.post(this.BASE_URL, data).then(() => {
            this.loadUsers()
        }).catch((error) => {
            console.error(error)
        })
    }

    handleDelete = (id) => {
        console.log(id)
        axios.delete(this.BASE_URL + "/" + id).then(() => {
            this.loadUsers()
        })
        this.setState({selectedUser: undefined})
    }

    render() {
        if (this.state.selectedUser) {
            var editLabel = <div>Editando {this.state.selectedUser.name}</div>
        } else {
            var editLabel = "Inserindo"
        }

        var userList = this.state.users.map((value) => {
            return <UserItem
                key={value._id}
                value={value}
                handleDelete={(id) => this.handleDelete(id)}
                handleSelect={(id) => this.selectUser(id)}
            />
        })

        let { name, password, _id } = this.state.selectedUser ? this.state.selectedUser : ""

        var userForm = <UserForm
            handleAction={(data) => this.handleAction(data)}
            key={_id}
            name={name}
            password={password}
        />

        return <div>
            <AppBar position="static">{editLabel}</AppBar>
            <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
            >
                {userForm}
            </Grid>

            <List
                subheader={
                    <ListSubheader component="div" id="nested-list-subheader">
                        Usuários
                    </ListSubheader>
                }
            >
                {userList}
            </List>
        </div>
    }
}

export default UserPage