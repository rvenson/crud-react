import React from 'react'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField'

class UserForm extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            name: this.props.name,
            password: this.props.password,
        }
    }

    handleChange = (event) => {
        const inputName = event.target.name
        const inputValue = event.target.value
        this.setState({ [inputName]: inputValue })
    }

    handleClick = () => {
        var data = { "name": this.state.name, "password": this.state.password}
        this.props.handleAction(data)
    }

    render() {

        return <form onSubmit={this.handleClick}>
        <div>
            <TextField
            id="filled-basic"
            label="Nome"
            margin="normal"
            variant="filled"
            name="name" 
            value={this.state.name}
            onChange={this.handleChange}
            />
        </div>
        <div>
        <TextField
          id="filled-basic"
          label="Senha"
          margin="normal"
          variant="filled"
          name="password"
          value={this.state.password}
          onChange={this.handleChange}
        />
        </div>
        <div>
        <Button color="primary" onClick={this.handleClick}>Confirmar</Button>
        </div>
        </form>
    }
}

export default UserForm